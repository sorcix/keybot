package main

import (
	"bytes"
	"crypto/sha512"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"time"

	"golang.org/x/crypto/openpgp"
)

const (
	maxBytes  = 10000            // The maximum number of bytes to download per HTTP request.
	userAgent = "KeyBot/1.0"     // The user agent to report in HTTP requests.
	sleep     = 10 * time.Second // The amount of time to wait between updates in daemon mode.
)

var (
	endline = []byte{'\n'}
)

// fetch downloads up to maxBytes bytes from given URL.
func fetch(address string) (data []byte, err error) {
	parsed, err := url.Parse(address)
	if err != nil {
		panic(err) // Unrecoverable error: address is set at compile time!
	}

	req := &http.Request{
		Method: http.MethodGet,
		URL:    parsed,
		Header: map[string][]string{
			"User-Agent": {userAgent},
			"Accept":     {"text/plain"},
		},
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil || resp.StatusCode != 200 || resp.ContentLength > maxBytes {
		if resp.Body != nil {
			resp.Body.Close()
		}
		return nil, errors.New("unable to connect to server")
	}

	defer resp.Body.Close()

	limitedBody := &io.LimitedReader{
		R: resp.Body,
		N: maxBytes,
	}

	data, err = ioutil.ReadAll(limitedBody)
	if err != nil || len(data) < 1 {
		return nil, errors.New("unable to download authorized keys from server")
	}

	return data, nil
}

// validate checks if given authorized_keys data is correctly signed with given PGP signature.
func validate(auth, sig []byte) (err error) {
	var (
		authReader = bytes.NewReader(auth)
		sigReader  = bytes.NewReader(sig)
		keyReader  = strings.NewReader(keyring)
		keys       openpgp.KeyRing
		entity     *openpgp.Entity
	)

	if keys, err = openpgp.ReadArmoredKeyRing(keyReader); err != nil {
		panic(err) // Unrecoverable error: keyring is set at compile time!
	}

	if entity, err = openpgp.CheckArmoredDetachedSignature(keys, authReader, sigReader); err != nil || entity == nil || len(entity.Identities) < 1 {
		return errors.New("wrong signature on downloaded authorized keys")
	}

	return nil
}

// read reads authorized_keys data from given path.
func read(path string) (cur []byte, err error) {
	file, err := os.OpenFile(path, os.O_RDONLY, 0)
	if err != nil {
		return []byte{}, nil
	}
	defer file.Close()

	if cur, err = ioutil.ReadAll(file); err != nil {
		return nil, fmt.Errorf("unable to read from %s", path)
	}

	return cur, nil
}

// write writes authorized_keys data to given path.
func write(path string, truncate bool, auth []byte) (err error) {
	flags := os.O_WRONLY | os.O_CREATE
	if truncate {
		flags = flags | os.O_TRUNC
	}

	file, err := os.OpenFile(path, flags, 0600)
	if err != nil {
		return fmt.Errorf("unable to open %s for writing", path)
	}
	defer file.Close()

	if len(auth) > 0 {
		n, err := file.Write(auth)
		if err != nil {
			return fmt.Errorf("unable to write to %s", path)
		}
		if n != len(auth) {
			return fmt.Errorf("incomplete write (%d%%) to %s", n/len(auth), path)
		}
		if auth[len(auth)-1] != endline[0] {
			file.Write(endline)
		}
	}

	return nil
}

func usage() {
	fmt.Printf("Usage: %s [options] path\n\n", os.Args[0])
	flag.PrintDefaults()
	os.Exit(0)
}

func main() {
	var (
		merge   bool
		daemon  bool
		source  bool
		version bool
	)

	flag.BoolVar(&merge, "merge", merge, "Merge with the existing contents of authorized keys file.")
	flag.BoolVar(&daemon, "daemon", daemon, "Run keybot in daemon mode instead of updating once.")
	flag.BoolVar(&source, "source", source, "Show authorized keys download URL and exit.")
	flag.BoolVar(&version, "version", version, "Show version information and exit.")
	flag.Usage = usage
	flag.Parse()

	switch {
	case version:
		fmt.Println(userAgent)
		return
	case source:
		fmt.Println(authorized)
		return
	case flag.NArg() < 1:
		usage()
		return
	}

	logger := log.New(os.Stdout, userAgent+" ", log.LstdFlags)

	if daemon {
		loop(flag.Arg(0), !merge, logger)
		return
	}

	if err := refresh(flag.Arg(0), !merge); err != nil {
		logger.Fatalf("Could not update keys: %s!", err.Error())
	}
}

// loop starts keybot in daemon mode
func loop(path string, enforce bool, logger *log.Logger) {
	logger.Println("Starting in daemon mode.")

	// Open a channel to catch interrupt signals
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	// Create a ticker channel
	ticker := time.NewTicker(sleep)

	next := true
	for next {
		select {
		case <-ticker.C:
			if err := refresh(path, enforce); err != nil {
				logger.Printf("Could not update keys: %s!", err.Error())
			}
		case <-interrupt:
			ticker.Stop()
			next = false
			break
		}
	}

	logger.Println("Shutting down.")
}

// refresh tries checks and updates the authorized keys file
func refresh(path string, enforce bool) (err error) {
	var (
		auth []byte
		sig  []byte
		cur  []byte

		authHash [sha512.Size]byte
		curHash  [sha512.Size]byte
	)

	// Load current authorized keys file.
	cur, err = read(path)
	if err != nil {
		return err
	}
	curHash = sha512.Sum512(cur)

	// Attempt to download and check the latest authorized keys.
	if auth, err = fetch(authorized); err != nil {
		return err
	}
	if sig, err = fetch(signature); err != nil {
		return err
	}
	if err = validate(auth, sig); err != nil {
		return err
	}
	authHash = sha512.Sum512(auth)

	// We can stop (in any case) if the authorized keys file is identical.
	if authHash == curHash {
		return nil
	}

	// Simply write the latest authorized keys file in case we're running in enforce mode.
	if enforce {
		return write(path, true, auth)
	}

	// Merge!
	auth = merge(cur, auth)
	authHash = sha512.Sum512(auth)

	// Check if we just did useless work
	if authHash == curHash {
		return nil
	}

	return write(path, false, auth)
}

// merge returns a byte slice containing public keys from both cur and auth.
func merge(cur, auth []byte) []byte {
	curLines := bytes.Split(cur, endline)
	authLines := bytes.Split(auth, endline)

	// Normalize the latest authorized keys
	for j := range authLines {
		authLines[j] = bytes.TrimSpace(authLines[j])
		if len(authLines[j]) < 1 || authLines[j][0] == '#' {
			authLines = append(authLines[:j], authLines[j+1:]...)
		}
	}

	for i := range curLines {

		// Normalize current authorized keys
		curLines[i] = bytes.TrimSpace(curLines[i])
		if len(curLines[i]) < 1 || curLines[i][0] == '#' {
			continue
		}

		// Add any missing authorized keys
		for j := range authLines {
			if bytes.Equal(curLines[i], authLines[j]) {
				authLines = append(authLines[:j], authLines[j+1:]...)
				break
			}
		}
	}

	authLines = append(curLines, authLines...)
	return bytes.Join(authLines, endline)
}
