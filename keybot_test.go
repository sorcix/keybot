package main

import (
	"bytes"
	"testing"
)

func TestMerge(t *testing.T) {

	data := [...]struct {
		Current    []byte
		Authorized []byte
		Merged     []byte
	}{
		// Everything is empty!
		{
			Current:    []byte(""),
			Authorized: []byte(""),
			Merged:     []byte(""),
		},
		// Current stays as it is
		{
			Current:    []byte("a\nb\nc\nd"),
			Authorized: []byte(""),
			Merged:     []byte("a\nb\nc\nd"),
		},
		// New key!
		{
			Current:    []byte("a\nb\nc\nd"),
			Authorized: []byte("1"),
			Merged:     []byte("a\nb\nc\nd\n1"),
		},
		// New and existing key
		{
			Current:    []byte("a\nb\nc\nd"),
			Authorized: []byte("c\n1\na"),
			Merged:     []byte("a\nb\nc\nd\n1"),
		},
		// Comment
		{
			Current:    []byte("# hello\na\n# world\nb\nc\nd"),
			Authorized: []byte("c\n1\nd"),
			Merged:     []byte("# hello\na\n# world\nb\nc\nd\n1"),
		},
		// Empty lines
		{
			Current:    []byte("\n\na\n\nb\nc\nd\n\n"),
			Authorized: []byte("1\na"),
			Merged:     []byte("\n\na\n\nb\nc\nd\n\n\n1"),
		},
		// Windows end lines?
		{
			Current:    []byte("a\r\nb\r\nc"),
			Authorized: []byte("1\nc"),
			Merged:     []byte("a\nb\nc\n1"),
		},
	}

	var merged []byte
	for i := range data {
		merged = merge(data[i].Current, data[i].Authorized)

		if !bytes.Equal(merged, data[i].Merged) {
			t.Logf("Have: %s", string(merged))
			t.Logf("Want: %s", string(data[i].Merged))
			t.Fatal("Wrong merged result!")
		}
	}
}
