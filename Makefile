GOBUILD := CGO_ENABLED=0 go build -a -ldflags '-w -s'

build: prepare
	$(GOBUILD)

test: prepare
	go test

prepare:
	go get golang.org/x/crypto/openpgp

.PHONY: prepare build
